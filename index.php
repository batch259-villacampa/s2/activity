<?php require_once("./code.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S02 - Activity</title>
</head>
<body>
    <h1>S02 - Repetition Control Structures and Array Manipulation</h1>
    <h2>Divisible of Five</h2>
    <pre><?php printDivisibleOfFive(); ?></pre>

    <h2>Array Manipulation</h2>
    <?php array_push($students, "John Smith"); ?>
    <p><?= var_dump($students) ?></p>
    <p><?= count($students) ?></p>
    <?php array_push($students, "Jane Smith"); ?>
    <p><?= var_dump($students) ?></p>
    <p><?= count($students) ?></p>
    <?php array_shift($students); ?>
    <p><?= var_dump($students) ?></p>
    <p><?= count($students) ?></p>

</body>
</html>