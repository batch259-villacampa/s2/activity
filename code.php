<?php

// Print divisible of 5
function printDivisibleOfFive(){
    $count = 0;
    for($i = 0; $i <= 1000; $i++){
        if($i%5 == 0 && $count <= 100){
            echo $i . ', ';
            $count++;
            if($count%30==0){
                echo '<br/>';
            }
        }
    }
}

// Array Manipulation

$students = [];